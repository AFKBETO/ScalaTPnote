import scala.util.Random

package TPNote {
    object Minesweeper{
        private var board: Array[Array[Cell]] = null
        private var _mineCount = 0
        private var _openCellCount = 0;

        def getRemainingCell: Int = Utils.getDimension(board)._1 * Utils.getDimension(board)._2 - _mineCount - _openCellCount

        /**
         * @param w the width of the board
         * @param h the height of the board
         * @param mineCount the number of mines
         * @return
         */
        def startGame(w: Int, h: Int, mineCount: Int): Unit = {
            // Initialize the display board with hidden cells
            _mineCount = mineCount
            board = Utils.initGame(w, h, mineCount)
        }

        /**
         * Display the board, and highlighting the interacted cell
         * @param x the x coordinate of the interacted cell, default -1 if no interacted cell
         * @param y the y coordinate of the interacted cell, default -1 if no interacted cell
         * @return
         */
        def display(x: Int = -1, y: Int = -1): Unit = {
            // check if the board is initialized
            if (board == null) return
            val separateLine = "-" * (board(0).length * 4 + 6)
            val horizontalCoordinate = (0 until board(0).length).map(_ % 10).mkString("  |(", ")|(", ")|")
            // display the remaining cells
            println(s"Remaining cell${ if (getRemainingCell > 1)  "s" else "" }: $getRemainingCell")
            println(horizontalCoordinate)
            for (a <- board.indices) {
                println(separateLine)
                // print the line that contains the interacted cell separately
                if (x != -1 && y != -1 && a == y) {
                    print(s"${a % 10})|");
                    for (b <- board(a).indices) {
                        // marking the interacted cell
                        if (b == x) {
                            print(s"(${board(a)(b)})|")
                        } else {
                            print(s" ${board(a)(b)} |")
                        }
                    }
                    print(s"(${a % 10}\n");
                } else { // print the other lines using mkString
                    println(board(a).mkString(s"${a % 10})| "," | ",s" |(${a % 10}"))
                }
            }
            println(separateLine)
            println(horizontalCoordinate)
        }

        /**
         * Interact with the cell at the given coordinates
         * @param x the x coordinate of the cell
         * @param y the y coordinate of the cell
         * @return true if the cell is not a mine, false otherwise
         * @throws IllegalArgumentException if the coordinates are out of the board
         */
        def interact(x: Int, y: Int): Boolean = {
            if (!Utils.isInside(board)(x, y)) throw new IllegalArgumentException("The case is not inside the board")
            // if the first case is a mine, we reset the board
            if (_openCellCount == 0) {
                if (board(y)(x).isMine) {
                    Utils.setFirstCase(x, y)
                    board = Utils.initGame(Utils.getDimension(board)._1, Utils.getDimension(board)._2, _mineCount)
                }
            }
            // if the cell is already opened, return true
            if (board(y)(x).isRevealed) return true
            board(y)(x).reveal()
            // if the cell is a mine, return false
            if (board(y)(x).isMine) false
            else {
                // else, open the cell and its neighbors if it is a 0
                _openCellCount += 1
                if (board(y)(x).toString == "0") {
                    Utils.getNeighbors(board)(x, y).foreach(a => interact(a._1, a._2))
                }
                true
            }
        }

        /**
         * End the game and reveal all mines
         * @return
         */
        def endGame: Unit = for (a <- board.indices) {
            for (b <- board(a).indices) {
                if (!board(a)(b).isRevealed && board(a)(b).isMine) board(a)(b).reveal()
            }
        }
    }
}