package TPNote

trait Cell{
    protected var _revealed = false
    def reveal(): Unit = _revealed = true
    def isRevealed(): Boolean = _revealed
    def isMine(): Boolean = this.isInstanceOf[Mine]
}
