package TPNote

import scala.annotation.tailrec
import scala.util.Random
import scala.io.StdIn.{readInt, readLine}

object Utils {
    private var _firstX = -1
    private var _firstY = -1
    
    // function to set the first case as free : -1 so a mine
    def setFirstCase(x: Int, y: Int): Unit = {
        _firstX = x
        _firstY = y
    }
    
    // function to get back dimensions of the array defined
    def getDimension[T](mat: Array[Array[T]]) = (mat(0).size, mat.size)

    def isInside[T](mat: Array[Array[T]])(x:Int, y:Int): Boolean = x >= 0 && x < getDimension(mat)._1 && y >= 0 && y < getDimension(mat)._2

    // function to get back the neighbors of a cell
    def getNeighbors[T](mat: Array[Array[T]])(x:Int, y:Int): Array[(Int, Int)] = {
        var neighbors: Array[(Int, Int)] = Array()
        for (i <- x - 1 to x + 1){
            for (j <- y - 1 to y + 1){
                if (isInside(mat)(i, j) && !(i == x && j == y)){
                    neighbors = neighbors :+ (i, j)
                }
            }
        }
        neighbors
    }

    // function to get back random coordinates x and y 
    private def randomCoordinate[T](mat: Array[Array[T]]): (Int,Int) = (Random.nextInt(mat(0).length), Random.nextInt(mat.length))

    // function to create mine randomly in the array
    @tailrec
    private def randomMine(mat: Array[Array[Int]]) : Unit = {
        // get coordinates randomly to position the mine
        val (x, y) = randomCoordinate(mat)
        // verification of the cell(x,y) in the array so if it's not free, we have to get back other coordinates 
        if (mat(y)(x) == -1 || (x == _firstX && y == _firstY)){
            randomMine(mat)
        } else {
            // and if it is, then we can position the mine
            mat(y)(x)  = -1
            // and increment neighbors cells to indicates that there is a mine near them
            getNeighbors(mat)(x,y).foreach((coordinates:(Int, Int)) => incrementTab(mat)(coordinates._1, coordinates._2))
        }
    }
    
    // function to increment a cell when it's different that -1
    private def incrementTab(mat: Array[Array[Int]])(x:Int, y:Int): Unit = {
        if (isInside(mat)(x, y) && mat(y)(x) != -1){
            mat(y)(x) += 1
        }
    }

    
    private def initCase(k: Int): Cell = if (k == -1) Mine() else new Empty(k)
    
    //function to initialize the game
    def initGame(w: Int, h: Int, mineCount: Int): Array[Array[Cell]] = {
        // creation of an array for the board of the game through variables of array's dimensions and the number of mine
        val board = Array.ofDim[Int](h, w)
        for(_ <- 0 until mineCount){
            randomMine(board)
        }
        board.map(_.map(initCase))
    }
}

