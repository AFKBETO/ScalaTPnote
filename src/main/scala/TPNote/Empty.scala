package TPNote {
    class Empty(var adjacentMineCount: Int) extends Cell {


        override def toString(): String = if (_revealed) adjacentMineCount.toString else " "

        def canEqual(other: Any): Boolean = other.isInstanceOf[Empty]

        override def equals(other: Any): Boolean = other match {
            case that: Empty =>
                (that canEqual this) &&
                    adjacentMineCount == that.adjacentMineCount
            case _ => false
        }

        override def hashCode(): Int = {
            val state = Seq(adjacentMineCount)
            state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
        }
    }
}