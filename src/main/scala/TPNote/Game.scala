import scala.io.StdIn.readLine

package TPNote {
  object Game extends App {
    // initialization of board array
    var sizes = Array(0, 0)
    while(sizes(0) < 1 || sizes(1) < 1){
      try {
        print("Please choose the size of the game board (Format: w h): ")
        // asking the user to give new sizes of the array
        sizes = readLine().split(" ").map(_.toInt)
        // if it's invalid
        if (sizes(0) < 1 || sizes(1) < 1) throw new Exception("Invalid size")
      } catch {
        case ex: Exception => println(s"Invalid input: ${ex.getMessage}.")
      }
    }
    val (w, h) = (sizes(0), sizes(1))
    
    // initializationof the number of mines
    var mineCount = 0
    while (mineCount == 0) {
      try {
        print("Please choose the number of mines: ")
        // asking the user to give a new number of the mines
        mineCount = readInt()
        // checking the number given
        if (mineCount >= w * h * 9 / 9) throw new Exception("Too many mines")
        if (mineCount < 1) throw new Exception("Too few mines")
        // if the number given is correct, we can start the game
        Minesweeper.startGame(h, w, mineCount)
      } catch {
        case ex: Exception => println(s"Invalid input: ${ex.getMessage}.")
              mineCount = 0
      }
    }
    Minesweeper.startGame(h, w, mineCount)

    var isNotLost = true
    var coordinates = Array(-1, -1)
    // while the user has not lost the game and some cells are remaining not visible
    while (isNotLost && Minesweeper.getRemainingCell > 0) {
      Minesweeper.display(coordinates(0), coordinates(1))
      print("Please indicate the coordinates of the cell you want to interact (Format: x y): ")
      try {
        coordinates = readLine().split(" ").map(_.toInt)
        isNotLost = Minesweeper.interact(coordinates(0), coordinates(1))
      } catch {
        case ex: Exception => println(s"Invalid input: ${ex.getMessage}.")
          coordinates = Array(-1, -1)
      }
    }
    Minesweeper.endGame
    Minesweeper.display(coordinates(0), coordinates(1))
    if (isNotLost) {
      println("You won!")
    } else {
      println("You lost!")
    }
  }
}


